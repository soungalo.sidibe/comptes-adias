/**
 * View Models used by Spring MVC REST controllers.
 */
package org.sms.api.web.rest.vm;
