package org.sms.cucumber.stepdefs;

import org.sms.AcgetwayApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = AcgetwayApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
