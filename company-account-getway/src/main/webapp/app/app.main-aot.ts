import { platformBrowser } from '@angular/platform-browser';
import { ProdConfig } from './blocks/config/prod.config';
import { AcgetwayAppModuleNgFactory } from '../../../../target/aot/src/main/webapp/app/app.module.ngfactory';

ProdConfig();

platformBrowser().bootstrapModuleFactory(AcgetwayAppModuleNgFactory)
.then((success) => console.log(`Application started`))
.catch((err) => console.error(err));
