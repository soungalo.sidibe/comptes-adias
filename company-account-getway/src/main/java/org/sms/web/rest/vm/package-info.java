/**
 * View Models used by Spring MVC REST controllers.
 */
package org.sms.web.rest.vm;
